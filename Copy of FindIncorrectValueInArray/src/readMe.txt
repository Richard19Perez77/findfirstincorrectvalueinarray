Given an array of at least one correct value and followed by a continuous set of incorrect values.
Find the breaking point where correct turns to incorrect.

The idea is to find the break point by keeping track of the low and high of the array index.
Do this until the low is the highest correct cases and the high is lowest incorrect place.

Using divide and conquer we can see that we can eliminate much of the array from testing.
If we can find values in the halfway points of the array that are correct or incorrect.
This will eliminate sections from testing by inferring all before the correct point are correct
as well as that all points after the incorrect point are incorrect.

The array has at least one correct and at least one incorrect value.
There are no incorrect values before a correct value and there are no correct values after an incorrect value.

The solution works as follows by keeping track of the index's of known valid and 
invalid until the are contiguous in array space, finally returning the high value.

int low
int high

findError(array a){
	low = 0
	high = a.length
	return startSearch(a)
}

startSearch(array a){
	while(high - low != -1){
		findInvalidCase(a)
	}
}

findInvalidCase(array a){
	int mid = (high + low) / 2
	boolean errorFound = isError(a, mid)
	if(!errorFound){
		while(!errorFound){
			low = mid
			mid = (low + high) / 2
			errorFound = isError(a, mid)
		}
		high = mid
	}else{
		high = mid
		findValidCase(a)
	}
}

findValidCase(array a){
	int mid = (low + high) / 2
	boolean found = isCorrect(a, mid);
	while(!found){
		mid = (low + high) / 2
		found = isCorrect(a, mid)
	}
	
	low = mid
}

isCorrect(array a, int index){
	boolean isCorrect
	// your validation logic goes here for the value at the index
	// after performing check and setting value into isCorrect
	return isCorrect;
}


isError(array a, int index){
	boolean isError
	// your validation logic goes here for the value at the index in array a
	// and setting value into isCorrect
	return isCorrect;
}

