package solution;

public class Solution {
	/**
	 * Given an array of values return the first occurrence of an invalid index
	 * value
	 * 
	 * @param a
	 *            the first occurrence of the invalid index value
	 * @return
	 */

	int low = 0;
	int high = 0;

	public int findError(char[] a) {
		int res = 0;
		if (a.length >= 2) {
			low = 0;
			high = a.length;
			res = startSearch(a);
		}
		return res;
	}

	private int startSearch(char[] a) {

		while (high - low != 1) {
			findInvalidCase(a);

			// if only invalid is at end this won't be needed
			if (high - low != 1)
				findValidCase(a);
		}

		return high;
	}

	private void findInvalidCase(char[] a) {
		// search the array in halves until found or out of halves
		int mid = (high + low) / 2;
		boolean errorFound = isError(a, mid);

		if (!errorFound) {
			// overwrite the minimum low
			// keep searching for the first error
			while (!errorFound) {
				low = mid;
				mid = (low + high) / 2;
				errorFound = isError(a, mid);
			}
			high = mid;
		} else {
			// search from start to half point for valid case
			high = mid;
			findValidCase(a);
		}
	}

	private void findValidCase(char[] a) {
		int mid = (low + high) / 2;

		boolean found = isCorrect(a, mid);
		while (!found) {
			mid = (low + mid) / 2;
			found = isCorrect(a, mid);
		}

		low = mid;
	}

	private boolean isCorrect(char[] a, int halfIndex) {
		char c = a[halfIndex];
		boolean isLowerCase = Character.isLowerCase(c);
		return isLowerCase;
	}

	private boolean isError(char[] a, int halfIndex) {
		char c = a[halfIndex];
		boolean isCapitol = Character.isUpperCase(c);
		return isCapitol;
	}
}