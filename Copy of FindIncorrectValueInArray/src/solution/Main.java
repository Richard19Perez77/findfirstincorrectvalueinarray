package solution;
import java.util.ArrayList;

public class Main {

	/**
	 * Find the break point where the lower case stops and upper case begins
	 * Print the start of the condition of Upper case letters.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Solution s = new Solution();

		ArrayList<char[]> al = new ArrayList<char[]>();
		char[] a = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'H' };
		al.add(a);
		char[] b = { 'a', 'b', 'c', 'd', 'e', 'f', 'G', 'H' };
		al.add(b);
		char[] c = { 'a', 'b', 'c', 'd', 'e', 'F', 'G', 'H' };
		al.add(c);
		char[] d = { 'a', 'b', 'c', 'd', 'E', 'F', 'G', 'H' };
		al.add(d);
		char[] e = { 'a', 'b', 'c', 'D', 'E', 'F', 'G', 'H' };
		al.add(e);
		char[] f = { 'a', 'b', 'C', 'D', 'E', 'F', 'G', 'H' };
		al.add(f);
		char[] f2 = { 'a', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
		al.add(f2);

		char[] g = { 'a', 'B', 'C', 'D', 'E', 'F', 'G' };
		al.add(g);
		char[] h = { 'a', 'b', 'C', 'D', 'E', 'F', 'G' };
		al.add(h);
		char[] i = { 'a', 'b', 'c', 'D', 'E', 'F', 'G' };
		al.add(i);
		char[] j = { 'a', 'b', 'c', 'd', 'E', 'F', 'G' };
		al.add(j);
		char[] k = { 'a', 'b', 'c', 'd', 'e', 'F', 'G' };
		al.add(k);
		char[] l = { 'a', 'b', 'c', 'd', 'e', 'f', 'G' };
		al.add(l);

		char[] m = { 'a', 'B' };
		al.add(m);

		char[] n = { 'a', 'B', 'C' };
		al.add(n);
		char[] o = { 'a', 'b', 'C' };
		al.add(o);

		ArrayList<Integer> resList = new ArrayList<>();

		resList.add(7);
		resList.add(6);
		resList.add(5);
		resList.add(4);
		resList.add(3);
		resList.add(2);
		resList.add(1);
		resList.add(1);
		resList.add(2);
		resList.add(3);
		resList.add(4);
		resList.add(5);
		resList.add(6);
		resList.add(1);
		resList.add(1);
		resList.add(2);
		int acc = 0;
		for (char[] cArr : al) {
			System.out.println(s.findError(cArr) == resList.get(acc));
			acc++;
		}
	}
}
